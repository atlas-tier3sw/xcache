# xcache

This tool sets up a local xrootd cache on the machine.  For complete options, 
  xcache -h


## Usage
### Start
To start an instance, (note options to join and restart) eg:
```
  xcache start -d /tmp/desilva/myProxyCache

  The following env variables will be created which you can use:
    ALRB_XCACHE_PROXY=root://localhost:<port>//
    ALRB_XCACHE_PROXY_REMOTE=root://<hostname>:<port>//
  insert them to any URL that starts with root://....; eg
    ${ALRB_XCACHE_PROXY}root://...
```
  Note that access only from localhost and the same domain are allowed.
 

### List
To see what xrootd instances are running:
```
  xcache list
  xcache list -d /tmp/desilva/myProxyCache
```

### Kill
To kill:
```
   xcache kill    (no args needed if it is the parent process)
   xcache kill -p <parentId of process that started>
   xcache kill -p all
``` 


## Advanced usage:

### Site Proxy Cache

If you plan to use this as a long-term cache for a site for all local users, 
   then you should:

#### Location
Define the env variable $TMPDIR to point to a stable local dir location 
    that is not /tmp.  This is where the .alrb work area will be created 
    for that user so that one will be able to run xcache and manage the 
    instances.

#### Valid Proxy
Provide a valid atlas voms proxy that is renewed automatically.

   The easy and less secure method is to generate a long lived no-voms proxy, 
    copy the file to a secure location, and then generate a 96h -voms=atlas 
    proxy from it everyday (cron).

   The more secure way is to get a host certificate for the machine running 
    xcache and  put that in a location and ensure the *.pem files are owned 
    by the user.  
   The user then delegates a long-lived proxy to a myproxy server.  
   On the machine which runs xcache, get the proxy from myproxy with 
    -voms=alas everyday (cron).
   This procedure is described in a shell script that you can copy and use:
      $ATLAS_LOCAL_ROOT_BASE/user/myproxydelegate-example.sh


