#! /bin/bash
#!----------------------------------------------------------------------------
#!
#! xrootdLocalCache.sh
#!
#! actions for a local xrootd cache
#!
#! Usage:
#!     xrootdLocalCache.sh -h 
#!
#! History:
#!   24Oct17: A. De Silva, First version
#!
#!----------------------------------------------------------------------------

alrb_progname=xcache

#!----------------------------------------------------------------------------
alrb_fn_xrootdLocalCacheHelp()
#!----------------------------------------------------------------------------
{
    \cat <<EOF

Usage: $alrb_progname <actions> [options]    

    This tool sets up a local caching mechanism for xrootd

    <actions> are one of these:
    help                     Show this help
    kill                     Stop all xrootd processes by user on host
    list                     List the xrootd processes
    start                    Start a xrootd local cache process
                              Only one xrootd process is allowed per session
    usage                    Print brief usage page

    Options are:

     Options for start:
     -b --background=<INT>   allow to run in background without parent
			       This will prevent it from being marked 
			       as orphaned for <INT> days
     -d --cacheDir           proxy cache dir to use (or \$ALRB_xrdcacheDir)
                              Default: $ALRB_xrdcacheDir
        --config=<STRING>    config file to use (or \$ALRB_xrdcacheConfig)
                              Default: $ALRB_xrdcacheConfig
     -C --container=<STRING> container to use for running xcache
        --cOpts=<STRING>     options to pass on to setupATLAS -c (container)
			      warning: singularity options -C and -p will fail
        --disklow=STRING     (xrootd pfc.diskusage lowwater mark)
                              default: $alrb_diskusage_low
			      purging stops when this mark is reached.
			      can be a fraction or suffixed by g or t (bytes)
        --diskhigh=STRING    (xrootd pfc.diskusage highwater mark)
			      default: $alrb_diskusage_high
			      purging starts when this mark is reached.
			      can be a fraction or suffixed by g or t (bytes)
     -i --ignore=<STRING>    ignore restrictions.  Values are comma delimited:
                              user: allow multiple users to share cacheDir
                              host: allow any host to share chareDir
                              Warning: use at your own peril !
     -j --join               join existing xrootd from another session
                              if xrootd configuration match.  Incompatible with
                              restart option.
     -o --option=<STRING>    options to pass to xrootd; enclose in 
     			      double-quotes
     -r --restart            kill existing xrootd process and start a new one
                              Incompatible with join option.  Only original
                              parent process can restart.  The same port
                              will be used.
     -x --xrootd=<STRING>    xrootd version (or \$ALRB_xrdcacheXrootd)
                              Default: $ALRB_xrdcacheXrootd

     Options for list:
     -d --cacheDir           proxy cache dir to use (or \$ALRB_xrdcacheDir)
                              Default: $ALRB_xrdcacheDir

     Options for kill:
     -p --ppid=<STRING>      xrootd ppid number or =all
                              Default: $ALRB_xrdcacheKill
     -w --wipeclean          remove the cache dir

     General options:
     -h --help               Print this help message
     -q --quiet              Print no output
        --noOrphanCheck      Skip cleanup of orphaned processes

  Note that any xcache command will remove all orphaned xrootd processes 
EOF
}


#!----------------------------------------------------------------------------
alrb_fn_getAbsPath()
#!----------------------------------------------------------------------------
{
    local alrb_rawPath=`\echo $1`
    local alrb_userDir=$2
    
    \echo $alrb_rawPath | \grep -e "^\./"  > /dev/null 2>&1
    if [ $? -eq 0 ]; then
	\echo $alrb_rawPath | \sed -e 's|^\.|'$alrb_userDir'|'
	return 0
    fi
    
    \echo $alrb_rawPath | \grep -e "^/"  > /dev/null 2>&1
    if [ $? -eq 0 ]; then
	\echo $alrb_rawPath
	return 0
    else
	\echo "${alrb_userDir}/${alrb_rawPath}"
	return 0
    fi
    
    return 0
}


#!----------------------------------------------------------------------------
alrb_fn_printSummaryHeader()
#!----------------------------------------------------------------------------
{
    \echo ""
    printf "%-20s %10s %-6s %10s %6s %6s %-20s\n" "start (expire)" "host" "port" "owner" "pid" "ppid" "cache"
    return 0
}


#!----------------------------------------------------------------------------
alrb_fn_xrootdLocalCacheClearDead()
#!----------------------------------------------------------------------------
{

    if [ "$alrb_noOrphanCheck" = "YES" ]; then
	\echo "noOrphanCheck set; will not delete orphaned processes."
    fi
    
    alrb_activeInstanceAr=()

    if [ "$alrb_noOrphanCheck" != "YES" ]; then
	if [ -d $alrb_tmpDirProcess ]; then
	    \find $alrb_tmpDirProcess  -mindepth 1 -type d -empty -mtime +1 -delete
	    \find $alrb_tmpDirProcess -xtype l -delete
	fi
	if [ -d $alrb_tmpDirAdmin ]; then
	    \find $alrb_tmpDirAdmin  -mindepth 1 -type d -empty -mtime +1 -delete
	    \find $alrb_tmpDirAdmin -xtype l -delete
	fi
	if [ -d $alrb_cacheDirInfo ]; then
	    \find $alrb_cacheDirInfo  -mindepth 1 -type d -empty -mtime +1 -delete
	    \find $alrb_cacheDirInfo -xtype l -delete
	fi
	
	local alrb_tmpVal
	local alrb_result
	local alrb_runningInstances=( `\find -L $alrb_tmpDirProcess -maxdepth 2 -mindepth 2 -name settings.sh | \sed -e 's|settings\.sh||g'` )
	for alrb_tmpVal in ${alrb_runningInstances[@]}; do
	    source $alrb_tmpVal/settings.sh
	    alrb_result=`pgrep -x xrootd | \grep -e $alrb_xrdPid 2>&1`
	    if [ $? -ne 0 ]; then
		if [ "$alrb_quiet" != "YES" ]; then
		    \echo "$alrb_progname cleaning missing xrootd process $alrb_xrdPid"
		fi
		source $alrb_tmpVal/cleanup.sh
	    fi
	done	
    fi

    local alrb_runningInstances=( `pgrep -x xrootd -u $alrb_whoami` )
    for alrb_tmpVal in ${alrb_runningInstances[@]}; do
	local alrb_active="NO"
	local let alrb_xrdSchema=1
	local alrb_settingDir=`\ps -p $alrb_tmpVal -o args --no-headers | \grep alrb | \sed -e 's|.*-c \(.*\)xrdcache.cfg.*|\1|g'`
	if [ -e "$alrb_settingDir/settings.sh" ]; then
	    source $alrb_settingDir/settings.sh
	    \ps -p $alrb_xrdPPid > /dev/null 2>&1
	    if [ $? -eq 0 ]; then
		alrb_active="YES"
	    fi
	fi
	
	if [ $alrb_xrdSchema -gt 1 ]; then
	    if [ $alrb_xrdExpired -ge $alrb_timestamp ]; then
		alrb_active="YES"
	    fi
	fi

	if [ "$alrb_active" = "YES" ]; then
	    if [ -e "$alrb_settingDir/settings.sh" ]; then
		alrb_activeInstanceAr=( "${alrb_activeInstanceAr[@]}" "$alrb_settingDir" )
	    fi
	elif [ "$alrb_noOrphanCheck" != "YES" ]; then
	    if [ "$alrb_quiet" != "YES" ]; then
		\echo "$alrb_progname removing orphaned xrootd process $alrb_tmpVal"
	    fi
	    if [ -e $alrb_settingDir/cleanup.sh ]; then
		source $alrb_settingDir/cleanup.sh
	    else
		kill $alrb_tmpVal
	    fi
	fi
    done

    return 0
}

#!----------------------------------------------------------------------------
alrb_fn_xrootdLocalCacheList()
#!----------------------------------------------------------------------------
{

    local alrb_instance
    if [ ${#alrb_activeInstanceAr[@]} -ne 0 ]; then
	\echo -n "xrootd processes for $alrb_whoami runing on $alrb_host : "
	alrb_fn_printSummaryHeader
	for alrb_instance in ${alrb_activeInstanceAr[@]}; do
	    \cat $alrb_instance/summary.txt
	done
    fi

    if [[ "$ALRB_xrdcacheDir" != "" ]] && [[ -d "$ALRB_xrdcacheDir" ]]; then
	local alrb_runningInstances=( `\find -L $alrb_cacheDirInfo -maxdepth 3 -mindepth 3 -name cleanup.sh` )
	if [ ${#alrb_runningInstances[@]} -ne 0 ]; then
	    \echo " " 
	    \echo "xrootd processes using $ALRB_xrdcacheDir : "
	    if [ -e $alrb_cacheDirInfo/lockInfo.sh ]; then
		source $alrb_cacheDirInfo/lockInfo.sh
		\echo -n "Locked by user $alrb_xrdLockOwner and host $alrb_xrdLockHost"
	    fi
	    alrb_fn_printSummaryHeader
	    \cat $alrb_cacheDirInfo/*/*/summary.txt | env LC_ALL=C \sort -u
	fi
    elif [[ "$ALRB_xrdcacheDir" != "" ]] && [[ ! -d "$ALRB_xrdcacheDir" ]]; then
	\echo " "
	\echo "cache dir $ALRB_xrdcacheDir has not been created yet."
    else
	\echo " "
	\echo "No cache dir specified to list. Run with -d option to see more."
    fi
    
    return 0
}

#!----------------------------------------------------------------------------
alrb_fn_xrootdLocalCacheStart()
#!----------------------------------------------------------------------------
{

    local alrb_tmpVal 
    local alrb_tmpValN
    local alrb_rc

    if [ -e "$alrb_tmpDirProcess/$alrb_parentPid/settings.sh" ]; then
	source $alrb_tmpDirProcess/$alrb_parentPid/settings.sh
	alrb_tmpVal=`pgrep -x xrootd | \grep -e $alrb_xrdPid 2>&1` 
	if [ $? -ne 0 ]; then
	    if [ "$alrb_quiet" != "YES" ]; then
		\echo "$alrb_progname cleanup of missing xroot $alrb_xrdPid process"
	    fi
	    source $alrb_tmpDirProcess/$alrb_parentPid/cleanup.sh
	elif [[ "$alrb_restart" != "YES" ]] && [[ "$alrb_join" != "YES" ]]; then
	    \echo "Error: $alrb_progname has other instance of xrootd running for this session"
	    alrb_fn_printSummaryHeader
	    \cat $alrb_tmpDirProcess/$alrb_parentPid/summary.txt | env LC_ALL=C \sort -u
	    \echo " "
	    \echo "       use restart or join options"
	    return 64
	fi
    fi

    source $ATLAS_LOCAL_ROOT_BASE/swConfig/functions.sh
    alrb_fn_getToolVersion xrootd "$ALRB_xrdcacheXrootd"
    if [ $? -ne 0 ]; then
	\echo "Error: $alrb_progname could not determine xrootd version"
	return 64
    fi
    let alrb_tmpValN=`alrb_fn_versionConvert xrootd $alrb_candVirtVersion`
    if [ $? -ne 0 ]; then
	\echo $alrb_tmpValN
	return 64
    elif [ "$alrb_tmpValN" -lt 40700 ]; then
	\echo "Error: $alrb_progname needs xrootd versions >= 4.7.0" 1>&2
	return 64
    fi

    if [ "$ALRB_xrdcacheDir" = "" ]; then
	\echo "Error: $alrb_progname cache dir needs to be specified" 2>&1
	return 64
    else 
	\mkdir -p $ALRB_xrdcacheDir
	if [ $? -ne 0 ]; then
	    \echo "Error: $alrb_progname could not create proxy cache dir $ALRB_xrdcacheDir" 1>&2
	    return 64
	fi    

	alrb_tmpVal=`\mktemp $ALRB_xrdcacheDir/dummyXrdTest.XXXXXX 2>&1`
	if [ $? -ne 0 ]; then
	    \echo "$alrb_tmpVal"
            \echo "Error: $alrb_progname could not write in $ALRB_xrdcacheDir"
	    return 64
	fi           
	\rm -f $alrb_tmpVal
    fi 

    alrb_fn_createWorkdir
    if [ $? -ne 0 ]; then
	return 64
    fi    

    local let alrb_expired=0
    if [ $alrb_background -gt 0 ]; then
	let alrb_expired=$alrb_timestamp+$alrb_background
    fi
    
    \rm -f $alrb_Workdir/startXrootdScript.sh
    touch $alrb_Workdir/startXrootdScript.sh

    local alrb_py3setup=""
    if [ ! -z "$ATLAS_LOCAL_PYTHON_VERSION" ]; then
	if [[ ! -z "$ALRB_USE_PY3" ]] && [[ "$ALRB_USE_PY3" = "YES" ]]; then
	    alrb_py3setup="eval source \$ATLAS_LOCAL_ROOT_BASE/packageSetups/localSetup.sh \\\"python $ATLAS_LOCAL_PYTHON_VERSION\\\"" 
	elif [[ ! -z "$ALRB_LOCAL_PY3" ]] && [[ "$ALRB_LOCAL_PY3" = "YES" ]] && [[ "$alrb_useContainer" = "" ]]; then
	    alrb_py3setup="eval source \$ATLAS_LOCAL_ROOT_BASE/packageSetups/localSetup.sh \\\"python $ATLAS_LOCAL_PYTHON_VERSION\\\"" 
	fi
    fi

    local alrb_tmpDirAdminCfg=$alrb_tmpDirAdmin
    if [ "$alrb_useContainer" = "" ]; then
    # need a pristine environment to do this, hence this complicated method ...
        env | \grep -e "^ALRB" -e "^ATLAS_LOCAL_" -e "^X509_USER_PROXY" -e "^RUCIO_ACCOUNT" -e "^TZ" |  env LC_ALL=C \sort | \awk '{print "export "$1""}' | \sed -e 's|=|="|g' -e 's|$|"|g' >> $alrb_Workdir/startXrootdScript.sh 
    else
	# we need to preserve original X509 path since the container vanishes
	#  after xcache starts
        env | \grep -e "^X509_USER_PROXY" |  env LC_ALL=C \sort | \awk '{print "export "$1""}' | \sed -e 's|=|="|g' -e 's|$|"|g' >> $alrb_Workdir/startXrootdScript.sh 
	alrb_tmpDirAdminCfg=`\echo $alrb_tmpDirAdmin | \sed -e 's|'$alrb_adminDir'|/xrda|g'`
    fi    
    \cat <<EOF >> $alrb_Workdir/startXrootdScript.sh
let alrb_backgroundDay=$alrb_backgroundDay

if [ -z "\$ALRB_availableTools" ]; then
  eval source \$ATLAS_LOCAL_ROOT_BASE/user/atlasLocalSetup.sh --quiet
fi
$alrb_py3setup
if [ \$? -ne 0 ]; then
  return 64
fi

eval source \$ATLAS_LOCAL_ROOT_BASE/packageSetups/localSetup.sh \"xrootd $ALRB_xrdcacheXrootd\" emi xcache $alrb_quietOpt
if [ \$? -ne 0 ]; then
  return 64
fi

alrb_usePort=""

# chedk proxy
voms-proxy-info -exists > /dev/null 2>&1
if [ \$? -ne 0 ]; then
    \echo "   xrootd local cache not started as there is no valid proxy."
    \echo "     Type \"voms-proxy-init -voms atlas\" first and retry."
    return 64
fi

\sed -e 's|#XRDCACHE_DIR#|$ALRB_xrdcacheDir|g' \
    -e 's|#XRDCACHE_ADMINPATH#|$alrb_tmpDirAdminCfg|g' \
    -e 's|#XRDCACHE_DOMAINNAME#|$alrb_domainname|g' \
    -e 's|#XRDCACHE_DISKLOW#|$alrb_diskusage_low|g' \
    -e 's|#XRDCACHE_DISKHIGH#|$alrb_diskusage_high|g' \
    $ALRB_xrdcacheConfig > $alrb_Workdir/xrdcache.cfg

if [ -d $alrb_cacheDirInfo ]; then 
  if [ -e $alrb_cacheDirInfo/lockInfo.sh ]; then
    source $alrb_cacheDirInfo/lockInfo.sh
    alrb_tmpVal=\`\echo \"$alrb_ignore\" | \grep -e "user" 2>&1\`
    if [[ \$? -ne 0 ]] && [[ "\$alrb_xrdLockOwner" != "$alrb_whoami" ]]; then
       \echo "Error: $alrb_progname only \$alrb_xrdLockOwner allowed access to $ALRB_xrdcacheDir"
       return 64
    fi
    alrb_tmpVal=\`\echo \"$alrb_ignore\" | \grep -e "host" 2>&1\`
    if [[ \$? -ne 0 ]] && [[ "\$alrb_xrdLockHost" != "$alrb_hostF" ]]; then
       \echo "Error: $alrb_progname only \$alrb_xrdLockHost allowed access to $ALRB_xrdcacheDir"
       return 64
    fi
  fi
fi

if [ -d $alrb_cacheDirInfoHost ]; then 
  alrb_runningInstances=( \`\find $alrb_cacheDirInfoHost -maxdepth 1 -mindepth 1 -type d\` ) 
  for alrb_tmpVal in \${alrb_runningInstances[@]}; do
    if [ ! -e \$alrb_tmpVal/settings.sh ]; then
      continue
    fi
    \diff -q \$alrb_tmpVal/xrdcache.cfg $alrb_Workdir/xrdcache.cfg 2>&1 > /dev/null
    if [ \$? -eq 0 ]; then
      source \$alrb_tmpVal/settings.sh
#      if [ \$alrb_xrdOwner = "$alrb_whoami" ]; then
        if [ "$alrb_join" = "YES" ]; then
          cd $alrb_cacheDirInfoHost
          if [ "\$alrb_xrdPPid" != "$alrb_parentPid" ]; then 
            \rm -f $alrb_parentPid
            ln -s \$alrb_xrdPPid $alrb_parentPid 
          fi
          \rm -f $alrb_tmpDirProcess/$alrb_parentPid
          ln -s $alrb_cacheDirInfoProcess $alrb_tmpDirProcess/
          return 40        
	elif [ "$alrb_restart" = "YES" ]; then
          if [ "\$alrb_xrdPPid" = "$alrb_parentPid" ]; then 
             alrb_usePort=\$alrb_xrdPort
             source $alrb_tmpDirProcess/\$alrb_xrdPPid/cleanup.sh
          else
             \echo "Error: $alrb_progname only original session can restart"
             return 64
          fi
        else
          \echo "Error: $alrb_progname xrootd is running already with same configuration." 
          \echo "       Use the -j option to join it"
          return 64
        fi
#      fi
    fi
  done
fi

\mkdir -p $alrb_cacheDirInfoProcess

if [ ! -e $alrb_cacheDirInfo/lockInfo.sh ]; then
  \echo "alrb_xrdLockOwner=$alrb_whoami; alrb_xrdLockHost=$alrb_hostF" >> $alrb_cacheDirInfo/lockInfo.sh 
fi

\cp $alrb_Workdir/xrdcache.cfg $alrb_cacheDirInfoProcess/

if [ "\$alrb_usePort" != "" ]; then
  alrb_port=\$alrb_usePort
else
  alrb_port=\`\$ALRB_XCACHE_SWPATH/getSocket.sh\`
fi

if [ "$alrb_quiet" = "YES" ]; then
  alrb_quietVal="2>&1 > /dev/null"
else
  alrb_quietVal=""
fi

xrootd -p \$alrb_port -c $alrb_cacheDirInfoProcess/xrdcache.cfg -b -n $alrb_name -s $alrb_cacheDirInfoProcess/pid.txt -l=$alrb_cacheDirInfoProcess/messages.log $alrb_option \$alrb_quietVal
alrb_rc=\$?

if [ \$alrb_rc -eq 0 ]; then
  \echo "xcache started successfully.  Messages logged in $alrb_cacheDirInfoProcess/messages.log"
  \echo " ALRB_XCACHE_MYPROCESS=$alrb_parentPid"
  \echo " ALRB_XCACHE_FILES=$alrb_cacheDirInfoProcess"
  \echo "To use, see https://gitlab.cern.ch/atlas-tier3sw/xcache"
  \echo "Set in your environment one of these:"
  \echo " export ALRB_XCACHE_PROXY=\"root://localhost:\$alrb_port//\""
  \echo " export ALRB_XCACHE_PROXY_REMOTE=\"root://$alrb_hostF:\$alrb_port//\""
  if [ \$alrb_backgroundDay -gt 0 ]; then
    \echo "This instance will run even without the parent for \$alrb_backgroundDay days"
  fi
else
   \cat $alrb_cacheDirInfoProcess/messages.log
fi

\echo "alrb_port=\$alrb_port" > $alrb_cacheDirInfoProcess/port.sh

return \$alrb_rc

EOF

    if [ "$alrb_useContainer" = "" ]; then
        if [ ! -z $HOME ]; then
	    env -i HOME=$HOME bash -l -c "source $alrb_Workdir/startXrootdScript.sh"	
	else		
	    env -i bash -l -c "source $alrb_Workdir/startXrootdScript.sh"	
	fi
    else
	local alrb_mountTmpdir=""
	if [ ! -z $TMPDIR ]; then
	    alrb_mountTmpdir="-m \"$TMPDIR\""
	fi
	local alrb_userProxyDir=""
	if [ ! -z $X509_USER_PROXY ]; then
	    alrb_userProxyDir="-m `\dirname $X509_USER_PROXY`"
	fi
	eval source \${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh -c $alrb_useContainer -r \"source $alrb_Workdir/startXrootdScript.sh\" -m \"$ALRB_xrdcacheDir\" -m \"$alrb_Workdir\" $alrb_mountTmpdir $alrb_userProxyDir -m $alrb_adminDir:/xrda "$alrb_cOpts" $alrb_quietOpt
    fi
    alrb_rc=$?

    if [ $alrb_rc -eq 40 ]; then
# jointed existing process
	return 0
    elif [ $alrb_rc -eq 0 ]; then

	\rm -f $alrb_tmpDirProcess/$alrb_parentPid
	ln -s $alrb_cacheDirInfoProcess $alrb_tmpDirProcess/

	\cp $alrb_Workdir/startXrootdScript.sh $alrb_cacheDirInfoProcess/

	alrb_tmpVal="/tmp/xrootd.${alrb_name}.env"
	if [ -e \$alrb_tmpVal ]; then
	    \cp $alrb_tmpVal $alrb_cacheDirInfoProcess/
	fi

	source $alrb_cacheDirInfoProcess/port.sh 
	local alrb_xrootPid=`\cat $alrb_cacheDirInfoProcess/pid.txt`	

	\rm -f $alrb_cacheDirInfoProcess/settings.sh 
	\cat <<EOF >> $alrb_cacheDirInfoProcess/settings.sh
let alrb_xrdSchema=2
alrb_xrdName=$alrb_name
let alrb_xrdTimestamp=$alrb_timestamp
alrb_xrdPort=$alrb_port
alrb_xrdHost=$alrb_host
alrb_xrdHostF=$alrb_hostF
alrb_xrdDomainname=$alrb_domainname
alrb_xrdOwner=$alrb_whoami
alrb_xrdPid=$alrb_xrootPid
alrb_xrdPPid=$alrb_parentPid
alrb_xrdCacheDir=$ALRB_xrdcacheDir
let alrb_xrdExpired=$alrb_expired
let alrb_xrdBackgroundDay=$alrb_backgroundDay
EOF
	
	\rm -f $alrb_cacheDirInfoProcess/settingsClear.sh 
	\cat <<EOF >> $alrb_cacheDirInfoProcess/settingsClear.sh 
unset alrb_xrdName alrb_xrdTimestamp alrb_xrdPort alrb_xrdHost alrb_xrdHostF alrb_xrdOwner alrb_xrdPid alrb_xrdPPid alrb_xrdCacheDir alrb_xrdDomainname
EOF
	
	printf "%-20s %10s %-6s %10s %6s %6s %-20s\n" "$alrb_timestampDay ($alrb_backgroundDay)" $alrb_host $alrb_port $alrb_whoami $alrb_xrootPid $alrb_parentPid $ALRB_xrdcacheDir > $alrb_cacheDirInfoProcess/summary.txt

	\rm -f $alrb_cacheDirInfoProcess/cleanup.sh  
	\cat <<EOF >> $alrb_cacheDirInfoProcess/cleanup.sh
kill $alrb_xrootPid
\rm -rf $alrb_cacheDirInfoProcess
\rm -rf $alrb_tmpDirProcess/$alrb_parentPid
\rm -rf ~/$alrb_name
\rm -rf /tmp/$alrb_name
\rm -rf $alrb_tmpDirAdmin/$alrb_name
EOF
    else
	\echo "Error: $alrb_progname xrootd startup failed."
    fi

    return $alrb_rc

}


#!----------------------------------------------------------------------------
alrb_fn_xrootdLocalCacheKill()
#!----------------------------------------------------------------------------
{

    local alrb_rc=0

    if [[ -z $ALRB_xrdcacheKill ]] || [[ "$ALRB_xrdcacheKill" = "none" ]]; then
	\echo "$alrb_progname kill needs a -p=[ppid | all] option"
	return 64
    elif [ "$ALRB_xrdcacheKill" = "all" ]; then
	for alrb_instance in ${alrb_activeInstanceAr[@]}; do	    
	    if [ "$alrb_quiet" != "YES" ]; then
	  	source $alrb_instance/settings.sh
		\echo "$alrb_progname killing xrootd $alrb_xrdPid"
	    fi
	    source $alrb_instance/cleanup.sh
	    if [ "$alrb_wipeclean" = "YES" ]; then
		\rm -rf $alrb_xrdCacheDir
	    fi
	done
    else
	if [ ! -d  $alrb_tmpDirProcess/$ALRB_xrdcacheKill ]; then
	    \echo "Error: $alrb_progname xrootd ppid $ALRB_xrdcacheKill does not exist"
	    return 64
	fi
	if [ "$alrb_quiet" != "YES" ]; then
	    source $alrb_tmpDirProcess/$ALRB_xrdcacheKill/settings.sh
	    \echo "$alrb_progname killing xrootd $alrb_xrdPid"
	fi
	source $alrb_tmpDirProcess/$ALRB_xrdcacheKill/cleanup.sh
	if [ "$alrb_wipeclean" = "YES" ]; then
	    \rm -rf $alrb_xrdCacheDir
	fi
    fi

    # need sleep for slow file systems
    sleep 1
    alrb_fn_xrootdLocalCacheClearDead
  
    return $alrb_rc
}


#!----------------------------------------------------------------------------
alrb_fn_createWorkdir()
#!----------------------------------------------------------------------------
{
    alrb_Workdir=`\mktemp -d $alrb_tmpDirWork/workXXXXXX 2>&1`
    if [ $? -ne 0 ]; then
	\echo "Error: $alrb_progname unable to create xrdcache tmp work dir in $alrb_tmpDirWork"
	return 64
    fi
    
    return 0
}


#!----------------------------------------------------------------------------
alrb_fn_cleanup()
#!----------------------------------------------------------------------------
{
    if [ "$alrb_Workdir" != "" ]; then
	\rm -rf $alrb_Workdir
    fi

    return 0
}


#!----------------------------------------------------------------------------
# main
#!----------------------------------------------------------------------------

alrb_currentDir=`pwd`

if [ -z $ATLAS_LOCAL_ROOT ]; then
    \echo "Error: You need to do setupATLAS first"
    exit 64
fi

alrb_xrdcacheConfigDefault="\$ALRB_XCACHE_SWPATH/cacheproxyTemplate.cfg"

alrb_shortopts="h,q,d:,c:,x:,r,p:,j,i:,o:,w,C:,b:" 
alrb_longopts="help,quiet,cacheDir:,config:,restart,xrootd:,ppid:,join,ignore:,option:,wipeclean,container:,cOpts:,disklow:,diskhigh:,background:,noOrphanCheck"
alrb_result=`getopt -T >/dev/null 2>&1`
alrb_opts=`$ATLAS_LOCAL_ROOT_BASE/utilities/wrapper_parseOptions.sh bash $alrb_shortopts $alrb_longopts $alrb_progname "$@"`
if [ $? -ne 0 ]; then
    \echo " If it is an option for a tool, you need to put it in double quotes." 1>&2
    exit 64
fi
eval set -- "$alrb_opts"

if [ -z $ALRB_xrdcacheXrootd ]; then
    export ALRB_xrdcacheXrootd="dynamic"
fi
if [ -z $ALRB_xrdcacheConfig ]; then
    export ALRB_xrdcacheConfig="$alrb_xrdcacheConfigDefault"
fi

if [ ! -z $ALRB_XCACHE_MYPROCESS ]; then
    export ALRB_xrdcacheKill=$ALRB_XCACHE_MYPROCESS
elif [ -z $ALRB_xrdcacheKill ]; then
    ALRB_xrdcacheKill="none"
fi

alrb_quiet=""
alrb_quietOpt=""
alrb_restart="NO"
alrb_join="NO"
alrb_ignore=""
alrb_option=""
alrb_wipeclean="NO"
alrb_useContainer=""
alrb_diskusage_low="0.45"
alrb_diskusage_high="0.5"
alrb_cOpts=""
alrb_adminDir="$ALRB_tmpScratch/xrdcache"
let alrb_backgroundDay=0
let alrb_background=0
alrb_noOrphanCheck=""

while [ $# -gt 0 ]; do
    : debug: $1
    case $1 in
        -h|--help)
            alrb_fn_xrootdLocalCacheHelp
            exit 0
            ;;
	-b|--background)
	    let alrb_backgroundDay=$2
	    let alrb_background=${alrb_backgroundDay}*86400
	    shift 2
	    ;;
	--disklow)
	    alrb_diskusage_low="$2"
	    shift 2
	    ;;
	--diskhigh)
	    alrb_diskusage_high="$2"
	    shift 2
	    ;;
        -d|--cacheDir)
	    export ALRB_xrdcacheDir=`alrb_fn_getAbsPath "$2" $alrb_currentDir`
	    shift 2
	    ;;
	-x|--xrootd)
	    export ALRB_xrdcacheXrootd="$2"
	    shift 2
	    ;;
        -c)
	    \echo "Warning: -c option is depreciated; use --config now."
	    \echo "         if you want containers, use -C"
	    export ALRB_xrdcacheConfig="$2"
	    shift 2
	    ;;
	--config)
	    export ALRB_xrdcacheConfig="$2"
	    shift 2
	    ;;
        -C|--container)
	    alrb_useContainer="$2"
	    shift 2
	    ;;
	--cOpts)	    
	    alrb_cOpts="$2"
	    shift 2
	    ;;
	-p|--ppid)
	    export ALRB_xrdcacheKill="$2"
	    shift 2
	    ;;
	-o|--option)
	    alrb_option="$2"
	    shift 2
	    ;;
	-q|--quiet)   
	    alrb_quiet="YES"
	    alrb_quietOpt="-q"
	    shift
	    ;;
	-i|--ignore)   
	    alrb_ignore="$2"
	    shift 2
	    ;;
	-j|--join)   
	    alrb_join="YES"
	    shift
	    ;;
	-r|--restart) 
	    alrb_restart="YES"
	    shift
	    ;;
	-w|--wipeclean)
	    alrb_wipeclean="YES"
	    shift
	    ;;
	--noOrphanCheck)
	    alrb_noOrphanCheck="YES"
	    shift
	    ;;
        --)

            shift
            break
            ;;
        *)
            \echo "Internal Error: option processing error: $1" 1>&2
            exit 1
            ;;
    esac
done


if [[ "$alrb_join" = "YES" ]] && [[ "$alrb_restart" = "YES" ]]; then
    \echo "Error: $alrb_progname join and restart are incompatible options"
    exit 64
fi

alrb_parentPid=$PPID
alrb_whoami=`whoami`
alrb_host=`hostname -s`
alrb_hostF=`hostname -f`
alrb_domainname=`hostname -d`
alrb_name="xrd_${alrb_whoami}_${alrb_host}_${alrb_parentPid}"

alrb_tmpDirWork="${alrb_adminDir}/work/$alrb_host"
alrb_tmpDirProcess="${alrb_adminDir}/processes/$alrb_host"
alrb_tmpDirAdmin="${alrb_adminDir}/admin"
alrb_cacheDirInfo="${ALRB_xrdcacheDir}/.alrb"
alrb_cacheDirInfoHost="${ALRB_xrdcacheDir}/.alrb/$alrb_host"
alrb_cacheDirInfoProcess="${ALRB_xrdcacheDir}/.alrb/$alrb_host/$alrb_parentPid"
\mkdir -p $alrb_tmpDirWork
\mkdir -p $alrb_tmpDirProcess
\mkdir -p $alrb_tmpDirAdmin

alrb_timestamp=`date -u +%s` 
alrb_timestampDay=`date -d @$alrb_timestamp +"%m%d%y-%H:%M" -u`
alrb_activeInstanceAr=()

alrb_fn_xrootdLocalCacheClearDead

if [ "$*" = "" ]; then
    alrb_fn_xrootdLocalCacheHelp
else
    alrb_result=`\echo $* | \cut -f 1 -d " "`    
    shift
    if [ "$alrb_result" = "help" ]; then
	alrb_fn_xrootdLocalCacheHelp
    elif [ "$alrb_result" = "start" ]; then
	alrb_fn_xrootdLocalCacheStart 
    elif [ "$alrb_result" = "kill" ]; then
	alrb_fn_xrootdLocalCacheKill
    elif [ "$alrb_result" = "list" ]; then
	alrb_fn_xrootdLocalCacheList
    elif [ "$alrb_result" = "usage" ]; then
	\echo "Please see https://gitlab.cern.ch/atlas-tier3sw/xcache"
    else
	alrb_fn_xrootdLocalCacheHelp
    fi    
    alrb_rc=$?
fi


alrb_fn_cleanup

exit $alrb_rc

