#!----------------------------------------------------------------------------
#!
#! do.sh
#!
#! Wrapper for xrootdLocalCache.sh
#!
#! Usage:
#!     do.sh -h
#!
#! History:
#!   02Nov17: A. De Silva, First version
#!
#!----------------------------------------------------------------------------

$ALRB_XCACHE_SWPATH/xrootdLocalCache.sh "$@"
alrb_rc=$?

unset ALRB_XCACHE_FILES
unset ALRB_XCACHE_PROXY
unset ALRB_XCACHE_PROXY_REMOTE
unset ALRB_XCACHE_MYPROCESS

if [ $alrb_rc -eq 0 ]; then
    alrb_tmpVal="$ALRB_tmpScratch/xrdcache/processes/`hostname -s`/$$"
    if [ -e $alrb_tmpVal/settings.sh ]; then
	source $alrb_tmpVal/settings.sh
        export ALRB_XCACHE_FILES="$alrb_xrdCacheDir/.alrb/$alrb_xrdHost/$$"
	export ALRB_XCACHE_PROXY="root://localhost:$alrb_xrdPort//"
	export ALRB_XCACHE_PROXY_REMOTE="root://$alrb_xrdHostF:$alrb_xrdPort//"
	if [ "$alrb_xrdHostF" != "`hostname -f`" ]; then
	    \echo "xcache: only $alrb_xrdDomainname domain allowed access"  
	fi
	if [ "$$" = "$alrb_xrdPPid" ]; then
	    export ALRB_XCACHE_MYPROCESS="$alrb_xrdPPid"
	fi
	source $alrb_tmpVal/settingsClear.sh
    fi
fi

unset alrb_tmpVal

return $alrb_rc

