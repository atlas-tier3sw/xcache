#!/bin/sh
#!----------------------------------------------------------------------------
#!
#! getSocket.sh
#!
#! gets a free socket
#!
#! Usage:
#!     getSocket.sh
#!
#! History:
#!   24Oct17: R. Walker, First version
#!
#!---------------------------------------------------------------------------- 

# -*- coding: utf-8 -*-

"exec" "$(  check_interpreter() { unalias $1 2> /dev/null; unset $1; ALRB_envPython=$(command -v $1); [ $ALRB_envPython ] && $ALRB_envPython -c 'import socket' > /dev/null 2>&1 && { echo $ALRB_envPython; unset ALRB_envPython; }; }; [ $ALRB_envPython ] && echo $ALRB_envPython || check_interpreter python || check_interpreter python3 || check_interpreter python2 || echo /usr/bin/python )" "-u" "-Wignore" "$0" "$@"

from __future__ import print_function

import socket
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind(('', 0))
addr = s.getsockname()
print(addr[1])
s.close()
